---
title: "Autobase"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

An experimental module that's used to rebase (follow?) multiple casually linked [Hypercore](hypercore.md) into a single, linearized Hypercore for multi-user collaboration.
