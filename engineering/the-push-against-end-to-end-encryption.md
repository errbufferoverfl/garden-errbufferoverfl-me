---
title: "The Push Against End-to-End Encryption"
subtitle: "Who Really Benefits?"
author:
  - errbufferoverfl
date: 2023-06-11T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Child Safety and Digital Ethics
  - Encryption and Privacy Advocacy
  - Surveillance and Government Policy
---

In recent months, Australian police and the Australian Centre to Counter Child Exploitation (ACCCE) have ramped up a fear-driven campaign, warning about the "sextortion" of teenagers and calling for weakened end-to-end encryption on digital platforms. While these calls may seem rooted in protecting young people, they echo tactics from the ongoing "crypto wars," with motives far removed from child safety.

At its core, this campaign resembles the EARN IT Act in the U.S., a legislative effort that pressures tech companies to limit encryption under the guise of child safety. By mandating “best practices” that often include removing encryption protections, the EARN IT Act threatens users' security and privacy across the board.

This push for weakened encryption, ostensibly “for the children,” carries significant risks beyond its stated aim. Vulnerable groups rely on secure communication, including survivors of domestic violence, journalists working under threat, and businesses that need data confidentiality. Weakening encryption doesn’t just affect criminals—it creates gaps that leave average citizens and sensitive information exposed to unwanted surveillance and potential cyberattacks.

Australia already has legislation that expands government access to encrypted data. The Telecommunications and Other Legislation Amendment (Assistance and Access) Act (TOLA) has empowered the government to demand that tech companies circumvent their own security protocols. This legislation has caused economic harm and trust issues in the business sector, as foreign partners are increasingly wary of sharing data with Australian firms under these conditions.

The issue of child safety online is complex and cannot be solved by holding platforms responsible for all user-generated content. In fact, research consistently shows that children most at risk of exploitation are those who have previously suffered abuse, often by people in positions of trust. Addressing these issues requires nuanced support, not blanket surveillance.

The ACCCE and police may claim their goal is child safety, but their push aligns more with expanding government surveillance capabilities. If this were truly about protecting children, the focus would shift to empowering them, increasing their self-determination, and offering better protections against family and domestic violence. End-to-end encryption is not the threat—it’s a lifeline for countless vulnerable people who depend on secure communication to stay safe.

- [Post on Twitter](https://twitter.com/errbufferoverfl/status/1646427897727688704?s=20)
- [Post on Mastodon](https://mastodon.errbufferoverfl.me/@errbufferoverfl/110190569356553694)
