---
title: Collapsology
subtitle: "Why Exposed RDP Isn't Your Biggest Threat"
author:
  - errbufferoverfl
date: 2021-09-10T12:41:28+10:30
date-modified: 2024-12-03T12:43:25+10:30
categories:
  - Conference Talks
  - Organizational Threats
  - Security Culture
draft: true
---

::: {.callout-note}

**03 December 2024**

This blog post has been migrated from **errbufferoverfl.me** to **garden.errbufferoverfl.me**.

:::

In September 2021, I presented a conference talk entitled *"Collapsology: Why Exposed RDP Isn't Your Biggest Threat."* In this talk, I discussed how to identify cultural threats in a rapidly evolving business landscape and how modern tools can help us detect these threats before they lead to security culture collapse.

## Slides

<div style="position: relative; width: 100%; height: 0; padding-top: 56.2500%; padding-bottom: 48px; box-shadow: 0 2px 8px 0 rgba(63,69,81,0.16); margin-top: 1.6em; margin-bottom: 0.9em; overflow: hidden; border-radius: 8px; will-change: transform;">
  <iframe loading="lazy" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; border: none; padding: 0;margin: 0;"
    src="https://www.canva.com/design/DAEn_kdzmTs/view?embed">
  </iframe>
</div>

[PyConline 2021 - Collapsology: Why your biggest threat isn’t exposed RDP](https://www.canva.com/design/DAEn_kdzmTs/view?utm_content=DAEn_kdzmTs&utm_campaign=designshare&utm_medium=embeds&utm_source=link) by errbufferoverfl  

- [PDF Download (without speaker's notes)](/downloads/conference-talks/2021-pyconline-snakeoilacademy/collapsology-why-your-biggest-threat-isnt-exposed-rdp-errbufferoverfl.pdf)  

## Security Culture Toolkit

The *People-Centric Security Toolkit* by [Lance Hayden](https://twitter.com/hay_lance) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

Although Lance's website went down in December 2020, it can still be accessed via the [Wayback Machine](https://web.archive.org/web/20200219183945/http://lancehayden.net/culture/).

The original toolkit files are available for download below:

- [Security Culture Diagnostic Survey](/downloads/security-culture-toolkit/SCDS.zip)  
- [Security FORCE Survey](/downloads/security-culture-toolkit/Security_FORCE_Survey.zip)  
- [Security FORCE Metrics](/downloads/security-culture-toolkit/Security_FORCE_Metrics.zip)  

## Resources

**Essential Cybersecurity Science**  
Josiah Dykstra · ISBN: 978-1-491-92094-7  
[Amazon](https://www.amazon.com.au/Essential-Cybersecurity-Science-Josiah-Dykstra/dp/1491920947/) · [Book Depository](https://www.bookdepository.com/Essential-Cybersecurity-Science-Josiah-Dykstra/9781491920947) · [Booktopia](https://www.booktopia.com.au/essential-cybersecurity-science-josiah-dykstra/book/9781491920947.html) · [O'Reilly](https://www.oreilly.com/library/view/essential-cybersecurity-science/9781491921050/)  

**People-Centric Security: Transforming Your Enterprise Security Culture**  
Lance Hayden PhD · ISBN: 978-0-071-84679-0  
[Amazon](https://www.amazon.com.au/People-Centric-Security-Transforming-Enterprise-Culture/dp/0071846778/) · [Book Depository](https://www.bookdepository.com/People-Centric-Security-Transforming-Your-Enterprise-Security-Culture-Lance-Hayden/9780071846776) · [Booktopia](https://www.booktopia.com.au/people-centric-security-lance-hayden/book/9780071846776.html) · [O'Reilly](https://www.oreilly.com/library/view/people-centric-security-transforming/9780071846790/)  

**Collapse: How Societies Choose to Fail or Succeed**  
Jared Diamond · ISBN: 978-0-143-11700-1  
[Amazon](https://www.amazon.com.au/Collapse-Societies-Choose-Succeed-Revised/dp/0143117009/) · [Book Depository](https://www.bookdepository.com/Collapse-Jared-Diamond/9780241958681) · [Booktopia](https://www.booktopia.com.au/collapse-jared-diamond/book/9780241958681.html)  

**Security Chaos Engineering**  
Aaron Rinehart & Kelly Shortridge · ISBN: 978-1-492-08034-3  
[O'Reilly](https://www.oreilly.com/library/view/security-chaos-engineering/9781492080350/)

## References

- [Apple's child protection features spark concern within its own ranks](https://reut.rs/3kSVJxG)  
- [Slack rolls back parts of its new DM feature over harassment concerns](https://bit.ly/3BHy1eG)  
