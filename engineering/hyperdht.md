---
title: "HyperDHT"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:02+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

A [DHT](/engineering/dht.md) powering [Hyperswarm](/engineering/hyperswarm.md), each server is bound to a unique key pair, where the client connects to the server using the servers public key.
