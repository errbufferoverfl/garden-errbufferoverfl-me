---
title: November 2024 Submission in Response to the Online Safety Amendment (Social Media Minimum Age) Bill
subtitle: Submission to the Senate
author:
  - errbufferoverfl
date: 2024-11-22T09:16:17+11:00
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Child Safety and Digital Ethics
  - Encryption and Privacy Advocacy
  - Government Submission
  - Online Safety Bill
  - Surveillance and Government Policy
---

<center><strong>Submission in Response to the Senate regarding the inquiry into the Online Safety Amendment (Social Media Minimum Age) Bill 2024, November 2024</strong></center>

<center><i>Prepared by Rebecca Trapani on Friday, 22 November 2024</i></center>

The proposed Online Safety Amendment (Social Media Minimum Age) Bill 2024 introduces significant regulatory changes under the guise of improving online safety for children. While protecting young Australians from online harm is a vital goal, the approach outlined in this Bill is deeply flawed and inconsistent with previous government positions.

It is also disheartening that, given the ongoing national conversation surrounding the Social Media Minimum Age and its far-reaching consequences, the Senate has allowed only 24 hours for public consultation. This limited timeframe demonstrates a lack of genuine interest in consulting the Australian public on such a critical matter. Recent Crikey reporting highlights that this rushed approach aligns with the Federal Government's broader strategy of prioritizing optics over substance.[^1] Internal emails from South Australian Premier Peter Malinauskas’ office and interviews with attendees at related events suggest these were carefully stage-managed to create “momentum” for the social media ban, rather than fostering meaningful dialogue about its implications.

Furthermore, this Bill directly contradicts the Federal Government's *Response to the Roadmap for Age Verification* published in August 2023,[^2] which stated:

> The Roadmap makes clear that a decision to mandate age assurance is not ready to be taken. Without the technology to support mandatory age verification being available in the near term, the Government will require industry to do more and will hold them to account.

At the time, the Government acknowledged that the technologies required to enforce age assurance were immature and presented privacy, security, and implementation challenges​. This Bill is effectively a backdoor to a mandatory social-media ID system for adults, not children. Any system designed to enforce an age restriction must verify whether every user is under or over 16. Consequently:

- Every Australian adult will be subject to mandatory age checks to participate in social media platforms.
- This approach demands that platforms collect and store sensitive personal information, directly increasing the risk of data breaches and surveillance.

The Government is fully aware that Australians do not want to give more personal and private information to platforms from which they already seek greater protection. Forcing Australians to submit to even more surveillance is an illogical and harmful response to the public's legitimate demands for improved privacy protections.

The Bill fundamentally conflates surveillance with safety, proposing measures that rely on intrusive monitoring rather than addressing the underlying complexities of online harm. Policies centered on surveillance, particularly those involving mandatory ID checks, fail to grapple with the nuanced and systemic challenges of protecting children online. Such approaches offer a false sense of security while introducing significant risks to privacy and personal freedoms.

Excluding children from online spaces through inflexible and poorly designed mechanisms is not a solution. These measures risk further isolating young people from the social, educational, and developmental opportunities that digital platforms provide. Online spaces are integral to modern society, offering children access to resources, information, and communities that are essential for their growth. By barring access indiscriminately, the Bill not only limits these opportunities but may also push children toward unregulated and less safe corners of the internet.

The Bill embodies a paternalistic approach to policymaking, driven by unelected bureaucrats and implemented without meaningful consultation with the public or experts. This top-down method undermines democratic principles and erodes public trust. Effective policies require collaboration and transparency, yet the rushed and opaque nature of this legislation suggests a lack of interest in engaging with the complexity of the issue or the voices of those directly impacted. Surveillance does not equate to safety, and excluding vulnerable populations from digital spaces is no way to build a better or more inclusive society.

Once again, the Government is rushing to push through a poorly-considered Bill during the final sitting days of the year, ignoring every opportunity for meaningful engagement with stakeholders. This pattern of behavior demonstrates a troubling disregard for the complexity of online safety challenges. Addressing the multifaceted risks children face online requires thoughtful, evidence-based approaches, yet this Bill oversimplifies the issue and imposes blanket measures that fail to consider the nuances of the problem.

The Government has repeatedly ignored input from individuals and experts who have, over many years, detailed why measures like those proposed in this Bill are destined to fail. These stakeholders have offered clear, practical alternatives that would more effectively address online safety while avoiding unnecessary harm to privacy and civil liberties. But, their advice continues to be dismissed or sidelined, suggesting that consultation processes are treated as a mere formality rather than an opportunity to inform better policy.

This disregard for evidence and meaningful dialogue reinforces a growing perception within professional circles that engaging through official channels is futile. Many in the field now believe that their efforts to provide constructive, well-informed feedback are wasted, as evidence-based advice is consistently overlooked in favor of politically expedient decisions.

## Recommendation

Reject the Bill in totality and refocus efforts on evidence-based, proportionate, and privacy-preserving solutions:

- Support initiatives that provide age-appropriate, inclusive, and stigma-free education about online safety, consent, and respectful relationships. Empower children with the skills and knowledge to navigate online spaces safely.
- Invest in tools that enable children to manage their own online safety, with guidance and support from parents and guardians. Shift the emphasis away from parents and guardians solely policing children's internet use.
- Recognize that current age verification and assurance technologies are immature, unreliable, and carry significant privacy and security risks. Avoid regulatory mandates that compel platforms to implement invasive or impractical technological solutions.

## Conclusion

As I write this submission from bed while unwell, I am struck by the futility of reiterating concerns and solutions that have already been presented to the Government countless times. Policymakers and experts have consistently demonstrated why mandatory age checks are unworkable, ineffective, and harmful. Despite this, these voices have been ignored.

There is nothing new to add because the reality is that this Bill cannot be salvaged. It should be rejected in totality. Rather than imposing rushed, reactionary legislation, the Government should commit to engaging meaningfully with stakeholders to address online safety challenges in a way that respects privacy, supports children, and upholds democratic principles.

I agree to my submission being made public under my name.

Yours sincerely,

Rebecca Trapani

[^1]: [Emails reveal how Labor engineered event to support its own teen social media ban](https://www.crikey.com.au/2024/11/21/teen-social-media-ban-jonathan-haidt-peter-malinauskas/)
[^2]: [Government response to the Roadmap for Age Verification](https://www.infrastructure.gov.au/sites/default/files/documents/government-response-to-the-roadmap-for-age-verification-august2023.pdf)
