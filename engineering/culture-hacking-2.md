---
title: "Culture Hacking II"
subtitle: "Building Resilient Security Cultures"
author:
  - errbufferoverfl
date: 2024-12-03T12:50:34+10:30
date-modified: 2024-12-04T10:00:17+10:30
categories:
  - Ethical Hacking Practices
  - Organizational Change
  - Security and Behavior
draft: true
---

::: {.callout-note}

**03 December 2024**

This blog post has been migrated from **errbufferoverfl.me** to **garden.errbufferoverfl.me**. It has been updated with new insights, improved clarity, and new thoughts about the original content.

:::

> Failure in any system is like a cliff in the dark—a precipice we can’t see until we’re tumbling over the edge. It’s that fear of the unseen, waiting just beyond our immediate awareness, that keeps us wandering cautiously. When it comes to organizational culture, the risks are just as hidden but no less perilous.

Welcome to part two of our series on culture hacking! If you missed the first post, [check it out here](/engineering/culture-hacking-1). In part one, we explored the origins of culture hacking, rooted in system thinking, and how culture functions as the "software of the mind." Whether we’re talking about individuals or organizations, culture drives behaviors and values. In this post, we’ll explore why culture hacking matters—not as a startup gimmick to justify ping-pong tables, but as a vital tool for creating resilient, security-conscious organizations.

## Why Does Culture Hacking Matter?

I’ve rewritten this post multiple times, distilling it into two key reasons why culture hacking is essential:

1. If you’re not hacking your organization’s culture, someone else likely is—and they may not have your best interests at heart.
2. Continuously improving and hacking culture makes organisations more resilient to external influences.

Let’s break these down further.

## If You’re Not Hacking the Culture, Someone Else Is

Culture doesn’t exist in isolation. Every organization has internal and external forces vying for influence. When culture is left to chance, those forces can create environments that prioritize speed over quality, deadlines over security, or convenience over ethics.

Here’s a scenario many security professionals will recognize: Three sprints go by, and security tasks are consistently deprioritized in favor of new features or looming deadlines. Developers, eager to meet deliverables and avoid management pushback, make compromises—pushing quick fixes over robust solutions, skipping code reviews, or ignoring "minor" vulnerabilities.

Over time, these choices accumulate. What was once a small trade-off snowballs into a larger problem: a backlog of vulnerabilities and a culture where security is seen as a "nice-to-have" rather than a core value. By the time the mandatory penetration test rolls around, you’re faced with a 30-finding report, and the team is scrambling to clean up a mess that took months—or even years—to create.

The truth is, it didn’t happen overnight. Every decision made along the way reflected the organisation’s culture in action. Without someone intentionally steering the ship, culture drifts toward complacency, making the job of securing the organisation exponentially harder.

This problem becomes even more pronounced when the scale of operations can exacerbate cultural drift. Many large organisations operate with a security engineer-to-engineer ratio of 1:100—or worse. In these environments, the sheer volume of teams, projects, and deliverables makes it nearly impossible for security professionals to maintain a consistent presence across the organisation.

When security engineers are stretched so thin, they can only focus on the highest-priority issues, leaving many teams to fend for themselves. Developers might be left without clear guidance, relying on outdated security practices or none at all. Security concerns that don’t appear "urgent" are postponed indefinitely, creating a backlog that grows with every sprint.

In these scenarios, security becomes siloed. Instead of being embedded in the organisation’s processes, it’s relegated to specific teams or projects, creating a disconnect between security goals and day-to-day operations. Developers may begin to see security as an external force—something imposed upon them rather than an integral part of their work.

### Why the Security Engineer Ratio Matters

When you’re working with a 100:1 ratio, a single security engineer might be responsible for supporting dozens of teams or hundreds of developers. This imbalance can lead to several challenges:

- Security teams often operate in firefighting mode, responding to incidents and vulnerabilities after they occur instead of proactively preventing them.
- Without dedicated security resources, developers are expected to make critical security decisions without the expertise or tools to do so effectively.
- Small security wins—like implementing guardrails, providing training, or automating checks—fall by the wayside because there aren’t enough hands to execute them.
- When security is consistently deprioritized, it signals to employees that it isn’t important. This perception can spread quickly, embedding itself in the organization’s culture.

Addressing this drift requires deliberate effort to embed security into the organization’s culture. This is where culture hacking becomes invaluable. By identifying small, actionable changes that align with the organization’s goals, you can create an environment where security is prioritized—even in resource-constrained environments.

Addressing this drift requires deliberate effort to embed security into the organization’s culture. This is where culture hacking becomes invaluable. By identifying small, actionable changes that align with the organization’s goals, you can create an environment where security is prioritized—even in resource-constrained environments.

### Technical Initiatives: Tools Alone Aren’t Enough

Technical initiatives such as enabling developers with tools and training, automating processes, embedding security champions, and building guardrails can vastly improve the resources and support available to engineers. These measures help reduce cognitive load, streamline workflows, and create an environment where security feels accessible rather than burdensome.

However, technical solutions alone won’t fix a poor security culture. If an organization has been operating with a weak or indifferent security culture for a while, these initiatives can be met with skepticism. Engineers might perceive them as yet another superficial attempt at "improvement"—like the ping-pong tables and beer taps we discussed in [Part 1](/engineering/culture-hacking-part-1/).

This skepticism is especially problematic when security teams operate with a "build it, and they will come" mindset. Rolling out tools, training, or processes without meaningful collaboration often leads to disengagement. Teams may ignore the initiatives or see them as extra work imposed on an already stretched schedule. Without genuine engagement, these programs risk becoming unused, forgotten, or actively resented.

## Building Resilience Through Culture

In tech, security is becoming more and more fundamental, wether it's ensuring the physical security of employees and their devices, protecting against digital and operational threats. Security culture ensures that everyone feels secure, valued and empowered to contribute to the organisation’s goals.

Achieving this though is not the is not the responsibility of a single department or individual. It's a shared responsibility, where every everyone within the organisation plays an active role in fostering a culture of security.

A shared responsibility model in security culture recognises that security isn’t just the job of the security & compliance teams—it’s an organisational value that everyone upholds.

Threats security can emerge at any point and often when least expected. A new vulnerability in software or hardware might be disclosed overnight, leaving systems exposed before teams can react. A trusted third-party vendor may be compromised without widespread media coverage, allowing threat actors to exploit supply chain vulnerabilities unnoticed. Even within the organization, risks can arise from technical failures, human error, or misconfigurations, compounding the challenges of staying secure. By adopting a shared responsibility model, organisations create a culture where everyone takes a proactive part in addressing security, increasing the likelihood that these threats are identified and addressed before they escalate into full-blown crises. This collective approach ensures a faster and more effective response, reducing the overall impact of emerging risks.

When individuals understand their role in maintaining security, they are more likely to take proactive steps to protect themselves and their colleagues. This sense of responsibility transforms security from an abstract concept managed by a distant team into a tangible, day-to-day priority. Empowered employees feel ownership of their actions, which fosters a mindset of accountability. They recognise that their decisions—whether it’s following best practices, reporting suspicious activity, or advocating for security improvements—directly contribute to the organisation’s safety and resilience.

This shift in perspective not only helps mitigate risks but also builds a culture of collaboration, where everyone understands that security is a shared effort. Employees become partners in safeguarding the organisation rather than passive participants, creating an environment where they are motivated to identify and address potential vulnerabilities. This collective approach not only strengthens the organisation’s defences but also reinforces trust and confidence across teams, as every individual sees how their contributions align with and support the larger security framework. Over time, this proactive mindset becomes ingrained, making security an integral part of the organisational culture.

This attitude is especially helpful to harness as organisations grow and the complexity of their operations and the scale of their infrastructure expand exponentially, making it unrealistic for a centralised security team to manage every aspect of security. The volume of applications, services, and third-party integrations multiplies, creating a vast attack surface that no single team can oversee effectively. This is where a shared responsibility model becomes indispensable. By embedding security responsibilities at every level—across teams, departments, and roles—organisations create a distributed framework that scales naturally alongside their growth.

When individuals and teams take ownership of security within their domains, the organisation benefits from a network of vigilant contributors who can address potential issues in real time. This decentralised approach not only makes the system more resilient to threats but also ensures that security is integrated into everyday workflows. Teams are empowered to make informed decisions that align with organisational priorities, reducing bottlenecks and freeing up security resources to focus on strategic initiatives.

### What a Shared Responsibility Model for Security Looks Like

In a shared responsibility model, security is woven into the fabric of the organisation. It is not something that happens in isolation but is integrated into day-to-day activities, decision-making processes, and workplace culture. This requires clear roles, accessible resources, and consistent communication.

**At the individual level**, employees are encouraged to recognise and report issues, follow security protocols, and look out for one another. For example, it can be engineering taking the responsibility for reporting a suspected security weakness in source code, asking security about a media report of a third-party being compromised, spending some of their learning time to learn about relevant (or interesting) security research.

**At the team level**, managers are instrumental in setting the tone for security. They lead by example, integrating security into team workflows and emphasising its importance during meetings and reviews. Managers can facilitate regular security training tailored to their team’s specific roles, such as secure coding practices for developers or compliance training for administrative staff. Or advocate to leadership to have security-oriented user stories prioritised during planning. By creating an environment where reporting vulnerabilities or raising security concerns is seen as a proactive contribution rather than a failure, managers help demystify security and make it a collaborative effort.

**At the organisational level**, leadership is responsible for embedding security into the company’s strategic framework. This includes establishing comprehensive security policies, investing in infrastructure like advanced threat detection systems, and conducting regular audits to assess and improve security posture. Leadership must also ensure that security considerations are factored into business decisions, whether launching a new product, adopting new technology, or entering new markets. By treating security as a core organisational value, leadership fosters a unified approach that aligns all levels of the organisation in protecting critical assets and maintaining trust with stakeholders.

### Teaching Shared Responsibility: Strategies and Approaches

Creating a shared responsibility model for security culture requires thoughtful education and engagement. People need to understand not only what is expected of them but also why their involvement is critical. While the approach will change from organisation to organisation and will depend a lot on where the culture currently is, here are some of the general steps that can be taken

1. **Start with the Why**
Helping people see the importance of security as a shared responsibility is the first step. This involves communicating the direct and indirect benefits of a strong security culture, such as reduced security incidents, increased velocity, and a more supportive work environment. Use real-world examples and stories to illustrate how shared responsibility has prevented incidents or created positive outcomes.

2. **Make Expectations Clear**
People need to understand their specific role in the security framework. Provide clear, actionable guidance on what shared responsibility looks like in their context. For example, in an engineering team, this might involve detailed training on identifying common vulnerabilities, securely handling API keys, and reporting suspicious activity. When roles and expectations are well-defined, individuals are better equipped to contribute meaningfully to the organisation’s overall security posture.

3. **Create Opportunities for Collaboration**
Security is most effective when it’s a collective effort. Encourage teams to work together to identify risks, solve problems, and improve processes. Regular security-focused meetings, cross-functional working groups, and open forums can help foster collaboration and shared ownership of security practices. For instance, pairing engineers and security engineers to work on secure design reviews not only strengthens the product but also builds mutual understanding and trust between teams.

4. **Reinforce Through Training and Practice**  
Security training should be continuous, practical, and relevant to employees' roles. Hands-on exercises, simulations, and real-world scenarios help reinforce key concepts and demonstrate the importance of shared responsibility. For example, while tabletop exercises can prepare teams for incident response. These activities not only build skills but also emphasise that security is a shared, ongoing commitment.

5. **Recognise and Reward Positive Behaviour**  
Acknowledging contributions to security culture reinforces shared responsibility. Celebrate individuals and teams who go above and beyond to prioritize security, such as reporting a vulnerability, implementing a security improvement, or responding effectively during a security event. Recognition can take many forms, including awards, public acknowledgment during meetings, or even a simple thank-you email. These gestures reinforce the importance of security and encourage continued engagement.

6. **Provide Feedback Loops**  
Feedback is essential for continuous improvement in security practices. Create avenues for open communication where employees feel comfortable sharing their experiences, raising concerns, or suggesting enhancements. For example, regular check-ins, anonymous surveys, or dedicated Slack channels can provide platforms for input. Actively listening to this feedback and acting on it not only improves security systems but also demonstrates that the organization values its employees’ insights and contributions.

## Overcoming Challenges in Shared Responsibility

While the benefits of shared responsibility are clear, implementing such a model isn’t without challenges. People may resist additional responsibilities, particularly if they perceive security as "someone else’s job"—most likely the job of the dedicated security team. This mindset is often reinforced by a lack of visibility into how their actions directly impact the organisation’s security posture. Knowledge gaps can further complicate the situation; employees may feel ill-equipped to handle security tasks or fear making mistakes. In addition, if there is a lack of trust in the organisation’s commitment to security, efforts to implement shared responsibility may be met with skepticism or apathy.

To overcome these challenges, it is essential to foster a culture of openness and support. A key starting point is to recognise and reinforce that while it might sometimes feel like engineers and security are on opposing teams, they are, in fact, on the same team—working toward the same vision that leadership has set out. Both groups ultimately want to deliver high-quality, secure products that protect the organisation and its users. Acknowledging this shared purpose can dissolve the "us versus them" mentality, creating a collaborative environment where differences can be put aside in favour of shared goals.

Clear communication is vital to bridging this gap. Security and engineering teams must work together to understand each other's challenges and align their priorities. This includes recognising that resource shortages, time constraints, and tight deadlines are not caused by engineers or security themselves but are often leadership-driven issues stemming from broader organisational strategies and resource allocations. Framing these constraints as systemic rather than personal helps both teams realise they are allies facing a common challenge, not adversaries.

When engineering and security teams combine their efforts, they can more effectively rally against leadership to advocate for changes that resolve these issues. Whether it’s pushing for more resources, better tools, extended project timelines, or clearer priorities, presenting a united front makes these teams harder to ignore. This solidarity also fosters a stronger case for leadership buy-in, as it demonstrates that the organisation’s core technical teams are aligned in their needs and vision.

In the meantime, it remains crucial to support individual and team efforts through training, resources, and empowerment. Providing hands-on workshops, role-specific training, and accessible documentation ensures that all employees feel confident in their ability to contribute to security. Framing security as an enabler, rather than an obstacle, further helps to dissolve resistance and encourages collaboration.

Leadership also has a critical role to play in fostering this shared responsibility. When leaders visibly prioritise security, participate in training, and advocate for the resources their teams need, it sends an important message that security is a core organisational value. This top-down commitment reinforces the idea that engineering and security teams are working toward the same goals, strengthening trust and collaboration across the board.

Finally, creating feedback loops helps to sustain progress and address any challenges that arise. Encouraging open, honest communication about security practices and systemic issues allows both teams to voice concerns and propose solutions. Acting on this feedback not only improves the organisation’s security posture but also strengthens the partnership between engineering and security, ensuring they can focus their efforts on rallying for the changes that matter most. Together, these teams can move beyond their differences to build a culture of shared responsibility, resilience, and mutual respect.

## Sustaining a Culture of Shared Responsibility

Building a shared responsibility model for security culture isn’t a one-time effort; it’s an ongoing process that demands sustained commitment, consistency, and vigilance. One of the biggest challenges is in ensuring that the model is not only made a strong part of the organisational culture but also authentically lived out. This means going beyond talking about values to consistently enacting them in day-to-day operations and decision-making. Employees and leaders alike need to see that these values aren’t just aspirational—they are actionable principles that guide behaviour at all levels.

A critical part of this process involves indoctrinating new employees and vaccinating current ones against internal and external dissent. Some individuals or groups may resist taking ownership of security responsibilities, either because they view it as someone else’s job or because they actively oppose changes that disrupt their status quo. Left unaddressed, this resistance can undermine the collective effort. 

In some cases, individuals may attempt to weaponise or manipulate organisational values for personal or political gain, framing them as obstacles to productivity or using them to attack others’ performance. When this happens, it’s vital to challenge this behaviour in a healthy, constructive way, ensuring that values are upheld and not distorted.

Periods of leadership change amplify these challenges. Leadership transitions are inherently sensitive times when new priorities and perspectives can ripple through the organisation. Leaders wield significant influence—not only over their direct teams but also in shaping the broader cultural landscape. If a new leader deprioritizes or dismisses shared security responsibilities, it sends a signal that these values are negotiable, emboldening dissenters and undermining progress. Worse, it risks eroding trust, as employees see inconsistencies between the values being espoused and the actions being taken.

To navigate these challenges, organisations must embed security as a shared value through deliberate, consistent action. Indoctrinating new employees begins with onboarding. From their first day, new hires should understand that security is integral to the organisation’s mission and that they play a key role in maintaining it. Training should go beyond technical skills to include the "why" behind security practices, showing how individual actions contribute to the organisation’s overall resilience. This helps align new employees with the organisation’s security culture from the start.

For current employees, vaccination against dissent requires ongoing engagement. Regular discussions, workshops, and interactive sessions can provide opportunities to reinforce shared values, address misconceptions, and encourage open dialogue. It’s not enough to simply state values in a meeting or document; they must be enacted visibly and consistently. Employees need to see leadership modelling these values, making decisions that reflect them, and addressing instances where they are not upheld.

When individuals or teams attempt to undermine or manipulate these values, it’s essential to respond constructively. Healthy challenges should focus on reframing the discussion, reinforcing the importance of shared responsibility, and emphasising the collective benefits of security. For example, if someone dismisses security initiatives as "slowing down the process," the response could include illustrating how these efforts prevent costly breaches or downtime, ultimately enabling smoother operations. This approach not helps to address the immediate issue but also strengthens the cultural foundation by demonstrating that values are non-negotiable and integral to success.

Leadership transitions are critical moments to solidify these principles. Incoming leaders should be briefed on the organisation’s security culture and provided with the tools and resources needed to champion it. Existing leaders and employees can act as stewards, ensuring that cultural values remain consistent during times of change. Reinforcing values during leadership transitions also helps build resilience, ensuring the organisation can adapt without losing its cultural identity.

By continuously reinforcing the importance of security, adapting to challenges, and enacting values consistently, organisations can create a culture where security isn’t just a policy—it’s a shared commitment. When individuals and teams feel empowered and accountable, and when values are upheld against resistance or manipulation, organisations become safer, more supportive environments. Shared responsibility becomes more than a strategy; it evolves into a foundation of trust and collaboration that strengthens the organisation against both internal and external pressures.

## Security as a Team Sport

The bottom line is that security is a team sport. When security and engineering teams work together as equals, they foster an environment where safety and innovation thrive hand in hand. In such a culture, security becomes a shared value rather than an isolated responsibility, and everyone contributes to a collective goal.

This collaboration goes beyond checking boxes or meeting compliance requirements—it creates a foundation of respect and trust. Security teams gain valuable insights from engineers who understand the systems they build, while engineers benefit from the expertise of security professionals who anticipate and mitigate risks. Together, these teams can identify vulnerabilities earlier, implement more effective solutions, and respond to emerging threats with agility and confidence.

This partnership also has the power to send an important message throughout the organisation: that security is not a barrier to progress but a key enabler of success. When everyone shares in the responsibility for security, it becomes embedded in the organisation’s structure, empowering individuals and teams to innovate without compromising safety.

By championing shared responsibility, organisations can transform security from a reactive function into a proactive, collaborative effort that strengthens resilience, fosters growth, and drives long-term success. The ultimate result is not only a more secure environment but also a culture where every individual feels empowered to contribute, innovate, and thrive. Together, security and engineering teams can achieve a balance that benefits the entire organisation, ensuring that security supports—not stifles—the mission and vision they share.
