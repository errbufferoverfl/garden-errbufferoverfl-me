---
title: "Holepunch"
subtitle: "Tools and Frameworks Powering Decentralized Communication and Web Development"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

[Holepunch](https://github.com/holepunchto) is a company and suite of tools that "equips developers with a powerful suite of independent components to effortlessly construct peer-to-peer applications."

A Holepunch stack can be broken up into three parts:

## Building Blocks

- [Hypercore](/engineering/hypercore.md)
- [Hyperbee](/engineering/hyperbee.md)
- [Hyperdrive](/engineering/hyperdrive.md)
- [Autobase](/engineering/autobase.md)
- [HyperDHT](/engineering/hyperdht.md)
- [Hyperswarm](/engineering/hyperswarm.md)

[keet.io](https://keet.io) is a peer to peer chat that is built entirely on Holepunch.

## Helpers

// TODO
