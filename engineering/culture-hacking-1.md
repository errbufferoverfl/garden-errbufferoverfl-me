---
title: "Culture Hacking I"
subtitle: "A Guide to Transforming Organizations from Within"
author:
  - errbufferoverfl
date: 2024-12-03T12:44:31+10:30
date-modified: 2024-12-04T10:29:49+10:30
categories:
  - Ethical Hacking Practices
  - Organizational Change
  - Security and Behavior
draft: true
---

::: {.callout-note}

**03 December 2024**

This blog post has been migrated from **errbufferoverfl.me** to **garden.errbufferoverfl.me**. It has been updated with new insights, improved clarity, and new thoughts about the original content.

:::

Heya 👋🏼! 

Welcome to the first post in a series on culture hacking! Your first question might be, "What exactly is culture hacking, and why should I care about it?" Let’s dive in and explore.

## 🧠 Systems Thinking and Culture Hacking

Culture hacking has its roots in systems thinking, a concept developed by the Systems Dynamics Group in 1956. Systems thinking emphasizes that a system behaves differently when isolated from its environment or other parts of the system. In this analogy, the system is an organization, and the organization’s culture is its operating system.

When we talk about organizational culture, we’re not referring to the values posted on an "About Us" page or shared during employee induction sessions. Instead, we mean the lived day-to-day behaviors and values that employees embody. These are the "rules of the system" that govern how people act within the organization.

While "culture hacking" might sound like the latest business buzzword, it’s a concept that predates modern corporate trends. Even before we had terms like "culture hacking," activists, artists, and innovators were influencing and shaping societal norms—often without realizing they were doing it.

## 🧑🏻‍💻 What Exactly is Culture Hacking?

Dutch social psychologist Geert Hofstede famously described culture as the "software of the mind." This software is shaped by the communities, resources, and cultures we’re exposed to as we grow. Like any software, it can be hacked, influenced, and changed when subjected to new stimuli.

At its core, culture hacking is about exposing individuals or organizations to new ideas or leveraging existing beliefs to drive change. If we view organizational culture as a kind of operating system, we can understand how it can be reprogrammed, much like software, to function differently.

So if we stick with idea that culture wether at an individual level, or organisational level is software, and the idea is that a system will act differently when isolated from the environment or other parts of the system, we can see how organisations can influence and change how we think, and how we can change organisations from the inside out.

However, this is easier said than done. Some organizational cultures are deeply entrenched and resistant to change, making them much harder to hack. The key is to find a weak point in the system—an area already susceptible to change—and start there.

![Photo of a dumpster fire, the dumpster is labelled organisational culture.](/imgs/dumpsterfire_meme.jpg)

## 🎯 The Strategy Behind Culture Hacking

Much like hacking a computer system, culture hacking is about targeting vulnerabilities. You wouldn’t attack the strongest part of an organization’s culture; instead, you’d focus on areas where resistance is low, and the culture is more malleable.

But hacking culture isn't the same as hacking computers. You can't just cycle through the options until you get the flag -- with each change you make no matter how big or small there are other changes further up and down the line that will influence and shape what your next option is.

![Fallout Dialogue Example](/imgs/fallout_3_dialogue_example.jpg)  

So you can't just restore your last save if you decide the outcome of a particular choice wasn't quite what you were after.

## 👥 Hacking People’s Culture: A Double-Edged Sword

The idea of hacking a person’s belief system might feel invasive, and it can be if misused. When the wrong person drives a culture hacking initiative, it can lead to a toxic workplace or exacerbate existing issues.

So as part of a good culture hacking problem, you should borrow the following considerations from system thinking when making decisions about how you want to change the culture of your business:

1. **Consider longand short-term consequences:** Every action has ripple effects—think beyond the immediate outcomes.
2. **Acknowledge unintended consequences:** Not every result can be predicted, but you should account for potential risks.
3. **Recognize complex cause-and-effect relationships:** Actions in one part of the system can have surprising impacts elsewhere.
4. **Adopt diverse perspectives:** Look at the problem through different lenses to understand its nuances.

Historically, culture hacking was rooted in activism, art, and fashion—fields that shaped societal norms in powerful ways. Today, businesses have co-opted these techniques, often in an attempt to seem more "human." Renaming HR departments to "People Operations" or "Human Capital" and adding ping pong tables or beer taps are shallow attempts to market their "cool" cultures. These surface-level changes, however, rarely address the deeper systemic issues.

## Why Care About Culture Hacking?

It’s easy to feel cynical about corporate culture, especially when it’s sold as a product. But culture hacking, when done thoughtfully, has the potential to make meaningful changes. Over the next posts in this series, I’ll explore:

- **Why culture hacking matters:** The risks and rewards of changing organizational norms.
- **How to assess your organization’s culture:** Tools for identifying vulnerabilities and opportunities.
- **How to influence culture positively:** Strategies for driving security-conscious behavior and creating a healthier workplace.
