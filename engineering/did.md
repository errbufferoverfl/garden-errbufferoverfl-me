---
title: "Decentralized identifier"
subtitle: "also known as DID"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Decentralized Identity
  - Distributed Web
  - Hypercore
  - Web3
---

Decentralized identifier (DIDs) identifies any subject (e.g., a person, organization, thing, data model, abstract entity, etc.) that the controller of the DID decides that it identifies.

The [W3C DID Working Group](https://www.w3.org/groups/wg/did/) has developed and approved a [W3C Recommendations doc](https://www.w3.org/TR/did-imp-guide/).
