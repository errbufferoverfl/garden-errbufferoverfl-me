---
title: "Feminist Principles for Privacy, Consent, and Anonymity"
subtitle: "Empowering Agency and Defending Rights in Digital Spaces"
author:
  - errbufferoverfl
date: 2023-06-11T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Digital Rights and Feminism
  - Empowering Marginalized Voices Online
  - Feminist Theory
  - Surveillance and Privacy Advocacy
---

The principles of **embodiment** look at diverse experiences and relationships as human beings embodying multiple identities and realities in “disembodied” online spaces.

## Consent

Women's agency lies in their ability to make informed decisions on what aspects of our lives we share online.

If social media wanted to they could embed consent into their product, but they don't and instead focus on extracting as much data as possible with little attention to who is posting data to whom.

In the *general* (read: Newscorp & Murdoch) media, we see a lack of outrage about a lack of transparency in privacy breaches.

This is also a core theme of *The Age of Surveillance Capitalism* and is core to maintaining [surveillance capitalism](surveillance-capitalism.md).

### Principle in Action

- [Terms of Service Didn't Read](https://tosdr.org/) provides a mechanism in which users can provide informed consent when using a website because they can understand **what** they are agreeing to.

## Privacy & Data

Everyone should have the right to privacy and to full control over personal data and information online at all levels. There is a staunch rejection of private and state data collection for profit and online user manipulation.

Surveillance is a patriarchal tool, used to control and restrict women's bodies, speech and activism. This type of control doesn't start at home, or end with the state and we must pay equal attention to the surveillance practices of individuals, the private sector, [the state](/notebook/the-state.md) and non-state actors.

## Memory

Memory is about having the right to exercise and retain control over our personal history and memory on the Internet. This principle emphasizes the ability to access, manage, and delete personal information, as well as the need for transparency about who has access to that data and under what conditions.

### Principle in Action

- [Making a feminist internet: Movement building in a digital age](https://www.genderit.org/node/5035/)
- [The Right to be Forgotten](https://en.wikipedia.org/wiki/Right_to_be_forgotten) feels like it in part embodies this principal.

## Anonymity

This principle is about the right to exist online anonymously, which is essential for freedom of expression, especially for those challenging norms around sexuality and gender identity or facing discrimination. Anonymity provides safety for women and queer individuals.

In previous years, Australia has made attempts to restrict online anonymity, such as proposing laws to link social media accounts to verified identities and introducing anti-encryption legislation that requires platforms to provide user data, undermining privacy and anonymity.

### Principle in Action

- [Tor](https://www.torproject.org/), a tool widely used to protect online anonymity, embodies the principle of defending the right to be anonymous. 

## Children

## Violence

## Resources

- [The Do-It-Yourself Feminist Internet: Cyber feminist actions from Latin America](https://feministinternet.org/index.php/en/resource/do-it-yourself-feminist-internet-cyber-feminist-actions-latin-america)
- [Big Data and Sexual Surveillance](https://feministinternet.org/en/resource/big-data-and-sexual-surveillance)
- [Don't Troll: Stop Trolling Women Journalists](https://feministinternet.org/en/resource/dont-troll-stop-trolling-women-journalists)
