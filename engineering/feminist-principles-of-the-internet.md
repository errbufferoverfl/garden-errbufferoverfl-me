---
title: "Feminist Principles of the Internet"
subtitle: "A Gender and Sexual Rights Framework for a More Inclusive Digital World"
author:
  - errbufferoverfl
date: 2023-06-11T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Digital Rights and Feminism
  - Empowering Marginalized Voices Online
  - Feminist Theory
  - Surveillance and Privacy Advocacy
source: https://feministinternet.org/en/page/about
---

The Feminist Principles of the Internet are a series of statements that offer a gender and sexual rights lens on critical Internet-related rights.

There are 17 principles in total, organised into 5 clusters:

- Access
- Movements
- Economy
- Expression
- [Embodiment](feminist-principles-embodiment.md)

They aim to provide a framework for women's movements to articulate and explore issues related to technology.
