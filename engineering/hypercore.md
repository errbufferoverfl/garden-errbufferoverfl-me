---
title: "Hypercore"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:56:26+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

A distributed, peer to peer, append-only log that can be used to create fast scalable applications without a backend.
