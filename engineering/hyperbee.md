---
title: "Hyperbee"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

An append-only [B-Tree](/engineering/b-tree.md) running on Hypercore. It provides a key-value store API with mechanisms that can be used for:

- Inserting and retrieving key-value pairs;
- Atomic batch insertion;
- Creation of sorted iterators.
