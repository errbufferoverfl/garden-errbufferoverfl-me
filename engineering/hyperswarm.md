---
title: "Hyperswarm"
author:
  - errbufferoverfl
date: 2023-06-10T23:30:00+09:30
date-modified: 2024-12-03T10:57:18+10:30
categories:
  - Distributed Web
  - Hypercore
  - Web3
---

A high-level API for finding and connecting to peers who are interested in a "topic".
