---
title: "The Notebook"
listing:
  contents:
    - notebook
    - engineering
  fields: [date, title]
  sort: "date desc"
  type: default
  sort-ui: false
  filter-ui: false
  categories: true
  feed:
    type: partial
comments: false
citation: false
disable_flesch: true
page-layout: full
repo-actions: false
title-block-banner: false
toc: false
---


