---
title: "About"
image: /imgs/profile.jpeg
about:
  template: jolla
  image-width: 80px
  links:
    - icon: chat-square-quote-fill
      text: Bluesky
      href: https://bsky.app/profile/errbufferoverfl.bsky.social
      rel: "me"
    - icon: mastodon
      text: Mastodon
      href: https://genericsocialmediapage.com/@errbufferoverfl
      rel: "me"
    - icon: postage-heart-fill
      text: Ko-Fi
      href: https://ko-fi.com/errbufferoverfl
      rel: "me"
    - icon: book
      text: Bookwyrm
      href: https://bookwyrm.social/user/errbufferoverfl
      rel: "me"
    - icon: gitlab
      text: GitLab
      href: https://gitlab.com/errbufferoverfl
      rel: "me"
    - icon: github
      text: Github
      href: https://github.com/errbufferoverfl
      rel: "me"
    - icon: bi-envelope-paper-heart-fill
      text: Email
      href: mailto:hi@errbufferoverfl.me
      rel: "me"
comments: false
citation: false
disable_flesch: true
page-layout: full
repo-actions: false
toc: false
---

Howdy, y'all! 👉🏻🤠👉🏻

I’m [errbufferoverfl](notebook/errbufferoverfl.md), also known as Bec (they/them). I’m a political thembo with a passion
for exploring the intersection of culture, politics, and InfoSec, and I’ve become increasingly involved in critiquing
digital policy in Australia.

I currently call Berrin home and dream of transitioning to life as a subsistence farmer on a microfarm—a humble
residential plot where I can cultivate food and live more sustainably. When I’m not daydreaming about farming, I’m an
enthusiastic zinester and artist, spending my time painting or creating zines that explore topics like tech policy,
queer identity, and the beauty and complexity of everyday life.

By day, I’m a security engineer focused on security partnerships, working within an app-based browser ecosystem to
enhance collaboration between engineering and security teams. My goal is to build long-term partnerships that foster
secure, resilient systems, though navigating this in corporate spaces is an ongoing challenge.

I’m also a Python programmer who enjoys tackling a wide range of problems, from automation to technical challenges,
whenever the need or curiosity arises.