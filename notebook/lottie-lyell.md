---
title: "Lottie Lyell"
subtitle: "Actress, screenwriter, editor, and director"
author:
  - errbufferoverfl
date: 2024-07-13T20:46:27+09:30
date-modified: 2024-12-04T10:26:19+10:30
categories:
  - Australian Cultural History
  - Pioneers of Australian Cinema
  - Women in Film
---

Lottie Lyell, born Charlotte Edith Cox, was an important figure in early Australian cinema. She was an actress, screenwriter, editor, and director, known for her extensive collaboration with [Raymond Longford](/notebook/raymond-longford.md). Lyell’s contributions to the film industry were groundbreaking, particularly at a time when women's roles in filmmaking were limited.

## Contributions

- **Acting**: Lyell starred in many of Longford's films, showcasing her versatility and talent. Notable performances include *The Sentimental Bloke* (1919) and *The Blue Mountains Mystery* (1921).

- **Screenwriting and Directing**: Beyond acting, Lyell co-wrote and directed several films with Longford, including *The Sentimental Bloke*. Her involvement behind the camera marked her as one of Australia's first female filmmakers.

- **Innovation**: Lyell's work often pushed the boundaries of the era's cinematic techniques and storytelling methods. Her creative input was instrumental in shaping the narratives and visual styles of their films.

## Legacy

Lyell's early death at the age of 35 due to tuberculosis cut short a promising career. Despite this, she remains a critical figure for her pioneering efforts in a male-dominated industry.

## Notable Films

- *The Sentimental Bloke* (1919)
- *On Our Selection* (1920)
- *The Blue Mountains Mystery* (1921)
